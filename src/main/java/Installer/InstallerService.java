package Installer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Service
public class InstallerService {

    private static final Logger logger = LoggerFactory.getLogger(InstallerService.class);

    private final InstallerRepository installerRepository;

    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Autowired
    public InstallerService(InstallerRepository installerRepository) {
        this.installerRepository = installerRepository;
    }

    public void addAdmin() {
        installerRepository.save(new User("ESBAdmin", encoder.encode(System.getenv("POSTGRES")), "Admin",
                true));
        logger.info("ESBAdmin user created");
    }

    public void addVars() throws NoSuchAlgorithmException {
            logger.info("Creating secret key");
            String key = generateSecretKey();
            logger.warn("Secret key generated, create environment variable CRYPTO_KEY:" + key);
    }

    private String generateSecretKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(256);
        SecretKey secretKey = keyGenerator.generateKey();
        byte[] rawData = secretKey.getEncoded();
        return Base64.getEncoder().encodeToString(rawData);
    }
}
